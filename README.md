# gnunet-hs dev env

Have this as your top-level directory, and `bindings-gnunet`, `gnunet`, etc. in here.

``sh
git clone git@gitlab.com:wldhx-gnunet/gnunet.git
git clone git@gitlab.com:wldhx-gnunet/gnunet-hs.git
# etc.
```

```sh
nix-shell
cabal new-test bindings-gnunet
```
