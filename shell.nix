let
  pkgs = import (builtins.fetchTarball {
    name = "nixpkgs-unstable-2019-04-08";
    # `git ls-remote https://github.com/nixos/nixpkgs-channels nixos-unstable`
    url = https://github.com/nixos/nixpkgs/archive/ca2ba44cab47767c8127d1c8633e2b581644eb8f.tar.gz;
    # `nix-prefetch-url --unpack <url>`
    sha256 = "1jg7g6cfpw8qvma0y19kwyp549k1qyf11a5sg6hvn6awvmkny47v";
  }) {
    overlays = [ (import ./overlay.nix) ];
  };
  gnunet = (pkgs.callPackage ./gnunet { }).gnunet-dev;
  ghc = pkgs.haskellPackages.ghcWithPackages (ps: with ps; [
          bindings-DSL c2hs c2hsc gnunet hspec hspec-discover text stm stm-containers list-t network-transport network-transport-tests network-transport-tcp cereal atomic-primops
          # distributed-process
        ]);
in
pkgs.stdenv.mkDerivation {
  name = "gnunet-hs-env";
  buildInputs = [ ghc gnunet pkgs.cabal-install ];
  shellHook = "eval $(egrep ^export ${ghc}/bin/ghc)";
}
